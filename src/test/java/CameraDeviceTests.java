/**
 * Created by glenn on 19.10.15.
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import de.glenniso.garmin.virbx.exceptions.CameraNotFoundException;
import de.glenniso.garmin.virbx.model.CameraDevice;
import de.glenniso.garmin.virbx.model.FileInfo;
import de.glenniso.garmin.virbx.responses.*;
import org.junit.Test;
import de.glenniso.garmin.virbx.utils.CameraDetector;
import de.glenniso.garmin.virbx.utils.CameraDeviceFactory;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;


public class CameraDeviceTests {

    // public String urlString = "http://10.68.102.107/virb";
    public String urlString = "http://192.168.2.2/virb";

    @Test
    public void getInstanceOfACameraTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice( url );

            assert cameraDevice.getUrl() == url;

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }

    }

    @Test
    public void getStatusOfACameraTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice( url );

            assert cameraDevice.getUrl() == url;


            Response r = cameraDevice.status();

            assert r.result == 1;

            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void getInfoOfACameraTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);

            assert cameraDevice.getUrl() == url;

            InfoResponse r = cameraDevice.deviceInfo();

            assert r.result == 1;

            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void getFeaturesOfACameraTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice( url );

            assert cameraDevice.getUrl() == url;

            FeatureResponse r = cameraDevice.features();

            assert r.result == 1;

            assert r.features.size() == 25;

            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void getFoundOfACameraTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);

            assert cameraDevice.getUrl() == url;

            FoundResponse r = cameraDevice.found();

            assert r.result == 1;


            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void livePreviewTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);

            assert cameraDevice.getUrl() == url;

            LivePreviewResponse r = cameraDevice.livePreview();

            assert r.result == 1;
            assert r.url != null;


            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }


    @Test
    public void locateTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);

            assert cameraDevice.getUrl() == url;

            LocateResponse r = cameraDevice.locate();

            assert r.result == 1;

            try {
                Thread.sleep( 1000 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            FoundResponse foundResponse = cameraDevice.found();
            assert foundResponse.result == 1;

            System.err.println("hua");

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void snapPictureTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);

            assert cameraDevice.getUrl() == url;

            SnapPictureResponse r = cameraDevice.snapPicture();

            assert r.result == 1;

            assert r.name != null;


            System.err.println("picture url: " + r.url );

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }

    @Test
    public void snapPictureAndRemoveAgainTest() {


        URL url = null;
        try {

            String myUrl = CameraDetector.detectCamera(InetAddress.getByName("192.168.2.1"));



            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(new URL(myUrl));

            SnapPictureResponse r = cameraDevice.snapPicture();

            assert r.result == 1;



            Response dr = cameraDevice.deleteFile(Arrays.asList(new URL(r.url)));

            assert dr.result == 1;


            MediaListResponse mlr = cameraDevice.mediaList();
            assert mlr.result == 1;

            FileInfo fi = mlr.getNewestFile();

            System.err.println(" file url: " + fi.url );

            assert  !( fi.url.equals( r.url ) );





            System.err.println("picture url: " + r.url );

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


    }




    @Test
    public void videoStartStopTest1() {


        URL url = null;
        try {

            url = new URL( urlString );
            CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);



            assert cameraDevice.getUrl() == url;

            StartRecordingResponse sr = cameraDevice.startRecording();

            assert sr.result == 1;

            try {
                Thread.sleep( 5000 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            StopRecordingResponse st = cameraDevice.stopRecording();

            System.err.println("date: " + st.fileInfo.date + "  url: " + st.fileInfo.url );

        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }


    @Test
    public void cameraDetectTest() {


        URL url = null;
        try {

            try {
                String urlString = CameraDetector.detectCamera(InetAddress.getByName( "192.168.2.1" ));


                assert urlString != null;

                url = new URL( urlString );


                CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice(url);


                LocateResponse locr = cameraDevice.locate();

                assert locr.result == 1;


                try {
                    Thread.sleep( 200 );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                FoundResponse fr = cameraDevice.found();
                assert fr.result == 1;




            } catch (UnknownHostException e) {
                e.printStackTrace();
            }






        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch ( CameraNotFoundException ee ) {
            ee.printStackTrace();
        }


    }



}
