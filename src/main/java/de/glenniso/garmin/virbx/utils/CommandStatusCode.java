package de.glenniso.garmin.virbx.utils;

/**
 * Created by glenn on 15.10.15.
 */
public class CommandStatusCode {

    public static final Integer OK = 200;

    public static final Integer FAIL = 500;

}
