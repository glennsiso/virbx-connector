package de.glenniso.garmin.virbx.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenn on 19.10.15.
 */
public class Commander {

    public static final ContentType mimeType = ContentType.create( "application/json" );






    public static HttpResponse createMediaListCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.mediaList +  "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createStopRecordingCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.stopRecording +  "\"}", mimeType )
        );

        return client.execute( post );
    }


    public static HttpResponse createStartRecordingCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.startRecording +  "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createSnapPictureCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.snapPicture +  "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createLocateCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.locate +  "\"}", mimeType )
        );

        return client.execute( post );
    }


    public static HttpResponse createLivePreviewCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.livePreview +  "\", \"" + Commands.streamType + "\":\"" + Commands.rtp + "\" }", mimeType )
        );

        return client.execute( post );
    }


    public static HttpResponse createFoundCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.found +  "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createFeatureCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.features +  "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createInfoCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.deviceInfo + "\"}", mimeType )
        );

        return client.execute( post );

    }

    public static HttpResponse createStatusCommand( HttpClient client, HttpPost post ) throws IOException {

        post.setEntity(
                new StringEntity("{\"" + Commands.command + "\":\"" + Commands.status + "\"}", mimeType )
        );

        return client.execute( post );
    }

    public static HttpResponse createDeleteFileCommand( HttpClient client, HttpPost post, List<URL> fileUrls ) throws IOException {

        if ( fileUrls == null ) {
            fileUrls = new ArrayList<URL>();
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append( "{\"" + Commands.command + "\":\"" + Commands.deleteFile + "\",\"" + Commands.files +"\":[" );

        int i = 0;
        for( URL url : fileUrls ) {

            i++;
            stringBuilder.append( "\"" ).append( url.toString() ).append( "\"" );
            if ( i < fileUrls.size() ) {
                stringBuilder.append( "," );
            }

        }

        stringBuilder.append( "]}" );


        post.setEntity(
                new StringEntity( stringBuilder.toString(), mimeType )
        );

        return client.execute( post );
    }

}
