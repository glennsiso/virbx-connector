package de.glenniso.garmin.virbx.utils;

/**
 * Created by glenn on 15.10.15.
 */
public class Commands {

    public static final String command            = "command";

    public static final String files              = "files";
    public static final String streamType         = "streamType";
    public static final String rtp                = "rtp";

    public static final String deleteFile         = "deleteFile";
    public static final String deviceInfo         = "deviceInfo";
    public static final String features           = "features";
    public static final String found              = "found";
    public static final String livePreview        = "livePreview";
    public static final String locate             = "locate";
    public static final String mediaDirList       = "mediaDirList";
    public static final String mediaList          = "mediaList";
    public static final String sensors            = "sensors";
    public static final String snapPicture        = "snapPicture";
    public static final String startRecording     = "startRecording";
    public static final String status             = "status";
    public static final String stopRecording      = "stopRecording";
    public static final String stopStillRecording = "stopStillRecording";
    public static final String updateFeature      = "updateFeature";

    public static final String media              = "media";




}
