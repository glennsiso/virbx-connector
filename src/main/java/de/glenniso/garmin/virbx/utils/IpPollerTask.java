package de.glenniso.garmin.virbx.utils;

import de.glenniso.garmin.virbx.exceptions.CameraNotFoundException;
import de.glenniso.garmin.virbx.model.CameraDevice;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import de.glenniso.garmin.virbx.responses.FoundResponse;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by glenn on 20.10.15.
 */
public class IpPollerTask implements Runnable {

    private static Log log = LogFactory.getLog(IpPollerTask.class);
    private String inetAddress;
    private FoundResponse foundResponse;

    public IpPollerTask( String inetAddress ) {
        this.inetAddress = inetAddress;
    }


    public void run() {

        try {

            if ( InetAddress.getByName( inetAddress ).isReachable( 700 ) ) {

                CameraDevice cameraDevice = CameraDeviceFactory.createCameraDevice( new URL( "http://" + inetAddress ) );

                this.foundResponse = cameraDevice.found();

                if ( foundResponse != null && foundResponse.result == 1 ) {
                    log.error( "found -------- : " + foundResponse.result );
                }

            } else {
                foundResponse = null;
            }


        } catch (MalformedURLException e) {

            foundResponse = null;
        } catch (CameraNotFoundException e) {

            foundResponse = null;
        } catch (UnknownHostException e) {
            foundResponse = null;
        } catch (IOException e) {
            foundResponse = null;
        }


    }

    public FoundResponse getFoundResponse() {
        return foundResponse;
    }

    public String getInetAddress() {
        return "http://" + inetAddress;
    }

}
