package de.glenniso.garmin.virbx.utils;

import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import de.glenniso.garmin.virbx.responses.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class JsonToResultObjectMapper {


    private static Log log = LogFactory.getLog(JsonToResultObjectMapper.class);




    public static MediaListResponse createMediaListResponse( String content ) {

        return new MediaListResponse( createMap( content ) );
    }



    public static StopRecordingResponse createStopRecordingResponse( String content ) {

        return new StopRecordingResponse( createMap( content ) );
    }

    public static StartRecordingResponse createStartRecordingResponse( String content ) {

        return new StartRecordingResponse( createMap( content ) );
    }


    public static SnapPictureResponse createSnapPictureResponse( String content ) {

        return new SnapPictureResponse( createMap( content ) );
    }

    public static LocateResponse createLocateResponse( String content ) {

        return new LocateResponse( createMap( content ) );
    }


    public static LivePreviewResponse createLivePreviewResponse( String content ) {

        return new LivePreviewResponse( createMap( content ) );
    }


    public static FoundResponse createFoundResponse( String content ) {

        return new FoundResponse( createMap( content ) );
    }


    public static FeatureResponse createFeatureResponse( String content ) {

        return new FeatureResponse( createMap( content ) );
    }


    public static InfoResponse createInfoResponse( String content ) {

        return new InfoResponse( createMap( content ) );
    }

    public static StatusResponse createStatusResponse( String content ) {

        return new StatusResponse( createMap( content ) );
    }



    public static DeleteResponse createDeleteResponse( String content ) {

        return new DeleteResponse( createMap( content ) );
    }




    private static Map<String,Object> createMap( String content ) {

        Map<String, Object> retMap = null;

        JSONParser j = new JSONParser();
        JSONObject o = null;
        try {

            o = (JSONObject) j.parse( content );
            retMap = new Gson().fromJson( o.toJSONString() , new TypeToken<HashMap<String, Object>>() {}.getType());

        } catch (ParseException e) {
            log.error( "failed to parse content: " + e.getMessage() );
        }

        return retMap;
    }

}
