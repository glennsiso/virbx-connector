package de.glenniso.garmin.virbx.utils;

import de.glenniso.garmin.virbx.exceptions.CameraNotFoundException;
import de.glenniso.garmin.virbx.model.CameraDevice;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by glenn on 19.10.15.
 */
public class CameraDeviceFactory {


    public static CameraDevice createCameraDevice( URL url ) throws MalformedURLException, CameraNotFoundException {

        return CameraDevice.createCameraDevice( url );
    }

}
