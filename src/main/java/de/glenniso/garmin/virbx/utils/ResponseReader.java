package de.glenniso.garmin.virbx.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by glenn on 19.10.15.
 */
public class ResponseReader {

    private static Log log = LogFactory.getLog(ResponseReader.class);


    public static String responseReader( HttpResponse httpResponse ) {

        String responseContent = "";


        BufferedReader rd = null;
        try {
            rd = new BufferedReader(
                    new InputStreamReader(
                            httpResponse.getEntity().getContent()
                    )
            );

            StringBuilder stringBuilder = new StringBuilder();
            String line = "";
            while ((line = rd.readLine()) != null) {

                stringBuilder.append(line);

            }

            responseContent = stringBuilder.toString();

        } catch ( IOException e ) {
            log.error( "failed to read http response: " + e.getMessage() );
        } catch ( UnsupportedOperationException uoe ) {
            log.error( "unsupported operation: " + uoe.getMessage() );
        }

        return responseContent;
    }


}
