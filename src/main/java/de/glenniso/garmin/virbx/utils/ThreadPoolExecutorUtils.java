package de.glenniso.garmin.virbx.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Created by glenn on 20.10.15.
 */
public class ThreadPoolExecutorUtils {

    private static Log log = LogFactory.getLog(ThreadPoolExecutorUtils.class);

    public static NotifyingBlockingThreadPoolExecutor
    createThreadPoolExecutor( int threadPoolSize ) {

        int poolSize = threadPoolSize;      // the count of currently paralellized threads
        //int poolSize = 1;

        int queueSize = 2 * threadPoolSize;    // recommended - twice the size of the poolSize
        //int queueSize = 1;

        int threadKeepAliveTime = 1;
        TimeUnit threadKeepAliveTimeUnit = TimeUnit.SECONDS;
        int maxBlockingTime = 10;
        TimeUnit maxBlockingTimeUnit = TimeUnit.MILLISECONDS;
        Callable<Boolean> blockingTimeoutCallback = new
                Callable<Boolean>() {

                    public Boolean call() throws Exception {
                        // log.error("*** Still waiting for task insertion... ***");
                        // nothing to be done here..
                        return true; // keep waiting
                    }
                };

        NotifyingBlockingThreadPoolExecutor threadPoolExecutor =
                new NotifyingBlockingThreadPoolExecutor( poolSize, queueSize,
                        threadKeepAliveTime, threadKeepAliveTimeUnit,
                        maxBlockingTime, maxBlockingTimeUnit,
                        blockingTimeoutCallback);

        return threadPoolExecutor;

    }

    public static NotifyingBlockingThreadPoolExecutor createThreadPoolExecutor() {

        return createThreadPoolExecutor( 400 );

    }

    public static void waiting( NotifyingBlockingThreadPoolExecutor
                                        executor, long timeToWait ) {

        if ( executor.getTasksInProcess() > 0 ) {

            try {
                long timeToStop = timeToWait;
                boolean done = false;
                do {
                    done = executor.await( 500, TimeUnit.MILLISECONDS);
                    log.debug("waiting for " +
                            executor.getTasksInProcess() + " remaining task(s) of " + executor.getRunnables().size());

                    if ( executor.getRunnables().size() == 1 ) {

                        //Runnable r = executor.getRunnables().get( 0 );
                        //log.error( "kk" );

                    }

                    // turn on, if threads should all stop after 6 sec, no matter if there are finished  timeToStop = timeToStop - 1;
                } while( !done && timeToStop > 0 );
            } catch (InterruptedException e) {
                log.debug(e);
            }
        }

    }

    public static void waiting( NotifyingBlockingThreadPoolExecutor
                                        executor ) {

        waiting( executor, 600 );

    }

}
