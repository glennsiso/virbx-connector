package de.glenniso.garmin.virbx.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import de.glenniso.garmin.virbx.responses.FoundResponse;

import java.net.InetAddress;


/**
 * Created by glenn on 19.10.15.
 */
public class CameraDetector {

    private static Log log = LogFactory.getLog(CameraDetector.class);



    /**
     * inetAddress must be the address from segment where we should detect camera address
     *
     * this method takes all 255 - 1 possibilities and tries each to ping and get a foundResponse
     * from camera. all is done in paralell.
     *
     * @param inetAddress
     * @return url string if camera is available there
     */
    public static String detectCamera( InetAddress inetAddress ) {

        String deviceIp = inetAddress.toString();
        String baseNet = "";
        String[] baseNetArray = deviceIp.split( "\\." );

        NotifyingBlockingThreadPoolExecutor executor = ThreadPoolExecutorUtils.createThreadPoolExecutor( 230 );

        if ( baseNetArray.length == 4 ) {

            baseNet = baseNetArray[ 0 ] + "." + baseNetArray[ 1 ] + "." + baseNetArray[ 2 ];

            if ( baseNet.startsWith( "/" ) ) {
                baseNet = baseNet.substring( 1 );
            }

            for ( int i = 1; i < 255; i++ ) {

                String current = baseNet + "." + i;

                if ( ! ( baseNetArray[ 3 ].equals( Integer.toString( i ) ) ) ) {

                    executor.execute( new IpPollerTask( current ) );

                }

            }


            ThreadPoolExecutorUtils.waiting( executor, 150 );

            for ( Runnable r : executor.getRunnables() ) {

                IpPollerTask task = ( IpPollerTask )r;

                if ( task != null ) {

                    FoundResponse foundResponse = task.getFoundResponse();

                    if ( foundResponse != null && foundResponse.result == 1 ) {
                        return task.getInetAddress();
                    }

                }

            }


        } else {
            log.debug( "failed to create base net" );
        }

        return null;

    }




}
