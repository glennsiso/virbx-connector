package de.glenniso.garmin.virbx.responses;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class StatusResponse extends Response {

    public double lastMediaEventTime;
    public double recordingTime;
    public double antSensor;
    public double btSensor;
    public double apiMax;

    public double totalSpace;
    public double btHeadset;
    public int    photosRemaining;
    public String wifiMode;
    public double wifiSensors;
    public int    photoCount;

    public double wifiSignalStrength;
    public double availableSpace;
    public double apiMin;
    public String state;
    public double recordingRemaining;
    public double batteryLevel;


    public StatusResponse( Map<String, Object> responseMap ) {

        super( responseMap );

        if ( responseMap == null ) {
            return;
        }

        lastMediaEventTime  = setDouble( responseMap.get("lastMediaEventTime") );
        recordingTime       = setDouble( responseMap.get("recordingTime") );
        antSensor           = setDouble( responseMap.get("antSensor") );
        btSensor            = setDouble( responseMap.get("btSensor") );
        apiMax              = setDouble( responseMap.get("apiMax") );
        totalSpace          = setDouble( responseMap.get("totalSpace") );
        btHeadset           = setDouble( responseMap.get("btHeadset") );
        wifiSensors         = setDouble( responseMap.get("wifiSensors") );
        wifiSignalStrength  = setDouble( responseMap.get("wifiSignalStrength") );
        availableSpace      = setDouble( responseMap.get("availableSpace") );
        apiMin              = setDouble( responseMap.get("apiMin") );
        recordingRemaining  = setDouble( responseMap.get("recordingRemaining") );
        batteryLevel        = setDouble( responseMap.get("batteryLevel") );

        photosRemaining     = setInt( responseMap.get( "photoRemaining" ) );
        photoCount          = setInt( responseMap.get( "photoCount" ) );

        wifiMode            = setString( responseMap.get( "wifiMode" ) );
        state               = setString( responseMap.get( "state" ) );

    }



}
