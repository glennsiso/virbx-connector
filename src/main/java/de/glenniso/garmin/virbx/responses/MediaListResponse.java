package de.glenniso.garmin.virbx.responses;

import de.glenniso.garmin.virbx.model.FileInfo;
import de.glenniso.garmin.virbx.utils.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class MediaListResponse extends Response {

    public List<FileInfo> files;

    public MediaListResponse( Map<String,Object> map ) {

        super( map );
        files = new ArrayList<FileInfo>();
        if ( map == null ) {
            return;
        }

        List fileList = getFileList( map );

        for ( Object o : fileList ) {

            if ( o instanceof Map ) {

                Map m = (Map)o;

                // we assume the key is a string
                String url = (String) m.get( "url" );

                int date = 0;
                Object dateObject = m.get( "date" );
                if ( dateObject instanceof Double ) {

                    date = ((Double)dateObject).intValue();

                }

                String mediaType = (String) m.get( "type" );

                String thumbUrl = (String) m.get( "thumbUrl" );

                files.add( new FileInfo(
                                url,
                                mediaType,
                                thumbUrl,
                                date
                ) );


            }

        }


    }

    public FileInfo getNewestFile() {

        FileInfo newestFile = new FileInfo( "","","", 0 );

        for ( FileInfo fileInfo : files ) {

            if ( fileInfo.type.equals( "video" ) && fileInfo.date > newestFile.date ) {
                newestFile = fileInfo;
            }

        }

        return newestFile;
    }

    private List getFileList( Map map ) {

        Object o = map.get( Commands.media );

        if ( o == null ) {
            return new ArrayList();
        }
        if ( !( o instanceof List ) ) {
            return new ArrayList();
        }

        return  ( List ) map.get( Commands.media );
    }

}
