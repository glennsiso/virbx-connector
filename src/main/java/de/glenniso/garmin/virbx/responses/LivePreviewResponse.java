package de.glenniso.garmin.virbx.responses;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class LivePreviewResponse extends Response {

    public String url;


    public LivePreviewResponse( Map<String,Object> map ) {

        super( map );

        if ( map == null ) {
            return;
        }

        url = setString( map.get( "url" ) );

    }

}
