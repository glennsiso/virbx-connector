package de.glenniso.garmin.virbx.responses;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class DeleteResponse extends Response {

    public DeleteResponse( Map<String,Object> map ) {
        super( map );
    }

}
