package de.glenniso.garmin.virbx.responses;

import java.util.List;
import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class InfoResponse extends Response {


    public String model;
    public double firmware;
    public String type;
    public String partNumber;
    public double deviceId;

    public InfoResponse( Map<String, Object> map ) {
        super( map );

        if ( map == null ) {
            return;
        }

        Object o = map.get("deviceInfo");
        if ( o == null ) {
            return;
        }

        if ( !( o instanceof List ) ) {
            return;
        }
        if ( ((List)o).size() != 1 ) {
            return;
        }

        Map m = ((Map) ((java.util.ArrayList)o).get(0));

        model = setString( m.get("de/glenniso/garmin/virbx/model") );
        firmware = setDouble(m.get("firmware"));
        type = setString( m.get( "type" ) );
        partNumber = setString( m.get( "partNumber" ) );
        deviceId = setDouble(m.get("deviceId"));

    }


}
