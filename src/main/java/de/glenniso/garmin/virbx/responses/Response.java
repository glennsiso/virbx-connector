package de.glenniso.garmin.virbx.responses;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class Response {

    private static Log log = LogFactory.getLog(Response.class);


    public int result;


    public Response( Map<String,Object> responseMap ) {

        result = 0;

        if ( responseMap == null ) {
            return;
        }

        Object o = responseMap.get( "result" );
        if ( o == null ) {
            return;
        }

        if ( ! ( o instanceof Double ) ) {
            return;
        }

        result = ((Double) o).intValue();

    }





    public double setDouble( Object o ) {

        double returnValue = 0;

        if ( o == null ) {
            return returnValue;
        }

        if ( ! ( o instanceof Double ) ) {
            return returnValue;
        }

        return ((Double) o).doubleValue();
    }

    public int setInt( Object o ) {

        int returnValue = 0;

        if ( o == null ) {
            return returnValue;
        }

        if ( ! ( o instanceof Double ) ) {
            return returnValue;
        }

        return ((Double)o).intValue();
    }

    public String setString( Object o ) {

        String returnValue = "";

        if ( o == null ) {
            return returnValue;
        }

        if ( ! ( o instanceof String ) ) {
            return returnValue;
        }

        return (String) o;
    }


    public Map getMapFor( String keyName, Map<String,Object> map ) {


        if ( map == null ) {
            return null;
        }

        Object o = map.get(keyName);
        if ( o == null ) {
            return null;
        }
        if ( !( o instanceof Map ) ) {
            return null;
        }

        return (Map)o;
    }




}
