package de.glenniso.garmin.virbx.responses;


import de.glenniso.garmin.virbx.model.Feature;
import de.glenniso.garmin.virbx.model.FeatureStatus;
import de.glenniso.garmin.virbx.model.FeatureType;
import de.glenniso.garmin.virbx.utils.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class FeatureResponse extends Response {


    public List<Feature> features;

    /*
    public Feature previewWhileRecording;
    public Feature locateCamera;
    public Feature units;
    public Feature gps;
    public Feature rotation2;
    public Feature microphoneAdvanced;
    public Feature recordingLED;
    public Feature tones;
    public Feature powerSave;
    public Feature wowlan;
    public Feature friendlyName;
    public Feature language;
    public Feature videoFormat;
    public Feature videoMode;
    public Feature videoModeFps;
    public Feature videoModeRes;
    public Feature fieldOfView;
    public Feature videoLoop;
    public Feature autoRecord;
    public Feature videoLensCorrection;
    public Feature photoMode;
    public Feature selfTimer;
    public Feature dataStamp;
    public Feature imageSize;
    public Feature photoLensCorrection;
    */


    public FeatureResponse( Map<String,Object> map ) {

        super( map );

        features = new ArrayList<Feature>();

        if ( map == null ) {
            return;
        }

        List featureList = getFeatureList( map );

        for ( Object o : featureList ) {

            if ( o instanceof Map ) {

                Map m = (Map)o;

                // we assume the key is a string
                String name = (String) m.get( "feature" );
                FeatureType featureType = null;

                Object type = m.get( "type" );
                if (  type instanceof Double ) {
                    int ft = ((Double)type).intValue();
                    if ( ft == 0 ) {
                        featureType = FeatureType.STATIC_FEATURE;
                    }
                    if ( ft == 1 ) {
                        featureType = FeatureType.SELECTABLE_FEATURE;
                    }
                    if ( ft == 2 ) {
                            featureType = FeatureType.BOOLEAN_FEATURE;
                    }

                }

                Object options = m.get( "options" );
                List<String> optionList = new ArrayList<String>();
                if ( options != null && options instanceof List ) {

                    for ( Object s : (List) options ) {

                        if ( s instanceof String ) {
                            optionList.add( (String) s );
                        }
                    }
                }


                Object value = m.get( "value" );
                String valueString = "";
                if ( value != null && value instanceof String ) {
                    valueString = (String) value;
                }

                Object enabled = m.get( "enabled" );
                FeatureStatus featureStatus = FeatureStatus.UNKNOWN;
                if ( enabled != null && enabled instanceof Double ) {

                    if ( ((Double)enabled).intValue() == 0 ) {
                        featureStatus = FeatureStatus.DISABLED;
                    } else if ( ((Double)enabled).intValue() == 1 ) {
                        featureStatus = FeatureStatus.ENABLED;
                    }

                }

                Object optionSummaries = m.get( "optionSummaries" );
                List<String> optionSummariesList = new ArrayList<String>();
                if ( optionSummaries != null && optionSummaries instanceof List ) {

                    for ( Object s : (List) optionSummaries ) {

                        if ( s instanceof String ) {
                            optionSummariesList.add( (String) s );
                        }
                    }
                }


                Feature ff = new Feature(
                        name,
                        featureType,
                        optionList,
                        valueString,
                        featureStatus,
                        optionSummariesList
                );

                features.add( ff );

            }

        }

    }


    private List getFeatureList( Map map ) {

        Object o = map.get( Commands.features );

        if ( o == null ) {
            return new ArrayList();
        }
        if ( !( o instanceof List ) ) {
            return new ArrayList();
        }

        return  ( List ) map.get( Commands.features );
    }
}
