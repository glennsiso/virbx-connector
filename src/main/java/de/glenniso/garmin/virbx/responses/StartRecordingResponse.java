package de.glenniso.garmin.virbx.responses;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class StartRecordingResponse extends Response {

    public StartRecordingResponse( Map<String, Object> map ) {

        super( map );

        if ( map == null ) {
            return;
        }

    }

}
