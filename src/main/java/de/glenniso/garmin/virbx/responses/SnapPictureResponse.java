package de.glenniso.garmin.virbx.responses;

import java.util.Map;

/**
 * Created by glenn on 19.10.15.
 */
public class SnapPictureResponse extends Response {

    public String name;
    public String thumbUrl;
    public String url;



    public SnapPictureResponse( Map<String,Object> map ) {

        super( map );

        if ( map == null ) {
            return;
        }

        Map mediaMap = getMapFor( "media", map );

        Object nameObject = mediaMap.get("name");
        Object thumbUrlObject = mediaMap.get( "thumbUrl" );
        Object urlObject = mediaMap.get( "url" );

        if ( nameObject != null && thumbUrlObject != null && urlObject != null
                && nameObject instanceof String && thumbUrlObject instanceof String && urlObject instanceof String ) {

            this.name = ( String ) nameObject;
            this.thumbUrl = ( String ) thumbUrlObject;
            this.url = ( String ) urlObject;

        }


    }

}
