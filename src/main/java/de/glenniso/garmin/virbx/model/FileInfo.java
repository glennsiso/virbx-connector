package de.glenniso.garmin.virbx.model;

/**
 * Created by glenn on 19.10.15.
 */
public class FileInfo {

    public String   url;
    public String   type;         // photo or video
    public String   thumbUrl;     // only for photos
    public int      date;

    public FileInfo(
        String url, String type, String thumbUrl, int date
    ) {

        this.url = url;
        this.type = type;
        this.thumbUrl = thumbUrl;
        this.date = date;

    }
}
