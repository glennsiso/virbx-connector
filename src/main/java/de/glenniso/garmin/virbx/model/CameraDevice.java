package de.glenniso.garmin.virbx.model;



import de.glenniso.garmin.virbx.exceptions.CameraNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import de.glenniso.garmin.virbx.responses.*;
import de.glenniso.garmin.virbx.utils.Commander;
import de.glenniso.garmin.virbx.utils.JsonToResultObjectMapper;
import de.glenniso.garmin.virbx.utils.ResponseReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by glenn on 15.10.15.
 */
public class CameraDevice {

    private static Log log = LogFactory.getLog( CameraDevice.class );


    private URL url;



    private CameraDevice( URL url ) throws MalformedURLException {

        if ( !( url.toString().endsWith( "/virb" ) )) {
            url = new URL( url.toString() + "/virb" );
        }

        this.url = url;

    }


    /**
     * if camera URL is known and camera is responding, this method will create
     * a camera device instance.
     *
     * @return
     */
    public static CameraDevice createCameraDevice( URL cameraAddress ) throws MalformedURLException, CameraNotFoundException {

        if ( cameraAddress == null ) {
            throw new CameraNotFoundException();
        }

        return new CameraDevice( cameraAddress );

    }





    public boolean deviceLiving() {


        return true;
    }






    /**
     * given a list of file uris, this method tries to delete these files from
     * current device
     *
     * if the device de.glenniso.garmin.virbx.responses with an error code, Response with result = 0
     * result = 1 otherwise
     *
     * @param fileUrls
     * @return response
     */
    public Response deleteFile( List<URL> fileUrls ) {

        Response statusResponse = new Response( null );

        statusResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createDeleteFileCommand(c, p, fileUrls);

            // 3.) read response
            String responseContent = ResponseReader.responseReader(httpResponse);

            // 4.) map response jspn to Response-Object
            statusResponse = JsonToResultObjectMapper.createDeleteResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return statusResponse;

    }

    public InfoResponse deviceInfo() {

        InfoResponse infoResponse = new InfoResponse( null );

        infoResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createInfoCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            infoResponse = JsonToResultObjectMapper.createInfoResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return infoResponse;

    }

    public FeatureResponse features() {

        FeatureResponse featureResponse = new FeatureResponse( null );

        featureResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createFeatureCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            featureResponse = JsonToResultObjectMapper.createFeatureResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return featureResponse;

    }

    public FoundResponse found() {

        FoundResponse foundResponse = new FoundResponse( null );

        foundResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createFoundCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            foundResponse = JsonToResultObjectMapper.createFoundResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return foundResponse;
    }

    public LivePreviewResponse livePreview() {

        LivePreviewResponse livePreviewResponse = new LivePreviewResponse( null );


        livePreviewResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createLivePreviewCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            livePreviewResponse = JsonToResultObjectMapper.createLivePreviewResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return livePreviewResponse;

    }

    public LocateResponse locate() {

        LocateResponse locateResponse = new LocateResponse( null );


        locateResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createLocateCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            locateResponse = JsonToResultObjectMapper.createLocateResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return locateResponse;

    }

    public void mediaDirList() {

    }



    public MediaListResponse mediaList() {

        MediaListResponse mediaListResponse = new MediaListResponse( null );

        mediaListResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createMediaListCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            mediaListResponse = JsonToResultObjectMapper.createMediaListResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mediaListResponse;
    }

    public void sensors() {

    }

    public SnapPictureResponse snapPicture() {

        SnapPictureResponse snapPictureResponse = new SnapPictureResponse( null );

        snapPictureResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createSnapPictureCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            snapPictureResponse = JsonToResultObjectMapper.createSnapPictureResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return snapPictureResponse;

    }

    public StartRecordingResponse startRecording() {

        StartRecordingResponse startRecordingResponse = new StartRecordingResponse( null );

        startRecordingResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createStartRecordingCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            startRecordingResponse = JsonToResultObjectMapper.createStartRecordingResponse(responseContent);


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return startRecordingResponse;


    }

    /**
     *
     * @return StatusResponse Object
     */
    public StatusResponse status() {

        StatusResponse statusResponse = new StatusResponse( null );

        statusResponse.result = 0;

        try {

            if ( url.toString().endsWith( "/" ) ) {
                url = new URL( url.toString() + "/" );
            }

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createStatusCommand( c, p );

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            statusResponse = JsonToResultObjectMapper.createStatusResponse( responseContent );


        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return statusResponse;
    }


    public StopRecordingResponse stopRecording() {

        StopRecordingResponse stopRecordingResponse = new StopRecordingResponse( null );

        stopRecordingResponse.result = 0;

        try {

            // 1.) create http post request
            HttpClient c = HttpClientBuilder.create().build();
            HttpPost p = new HttpPost( this.url.toString() );

            // 2.) create Command (Http Entity)
            HttpResponse httpResponse = Commander.createStopRecordingCommand(c, p);

            // 3.) read response
            String responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            stopRecordingResponse = JsonToResultObjectMapper.createStopRecordingResponse(responseContent);



            // here we are a little better than the original api, we just asks for the latest video url and append it to the response
            // 2.) create Command (Http Entity)
            httpResponse = Commander.createMediaListCommand(c, p);

            // 3.) read response
            responseContent = ResponseReader.responseReader( httpResponse );

            // 4.) map response jspn to Response-Object
            MediaListResponse mediaListResponse = JsonToResultObjectMapper.createMediaListResponse(responseContent);

            stopRecordingResponse.fileInfo = mediaListResponse.getNewestFile();




        } catch ( MalformedURLException mue ) {
            log.error( "url malformed: " + mue.getMessage() );
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stopRecordingResponse;


    }

    public void stopStillRecording() {

    }

    public void updateFeature() {

    }

    public URL getUrl() {
        return url;
    }
}
