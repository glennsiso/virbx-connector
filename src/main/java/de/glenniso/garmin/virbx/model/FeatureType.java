package de.glenniso.garmin.virbx.model;

/**
 * Created by glenn on 19.10.15.
 */
public enum FeatureType {

    STATIC_FEATURE,
    SELECTABLE_FEATURE,
    BOOLEAN_FEATURE

}
