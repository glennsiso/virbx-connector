package de.glenniso.garmin.virbx.model;

import java.util.List;

/**
 * Created by glenn on 19.10.15.
 */
public class Feature {

    public String name;
    public FeatureStatus status;

    public List<String> options;

    public List<String> optionSummaries;

    public FeatureType type;

    public String value;


    public Feature(
            String name,
            FeatureType featureType,
            List<String> optionList,
            String valueString,
            FeatureStatus status,
            List<String> optionSummariesList ) {

        this.name = name;
        this.type = featureType;
        this.options = optionList;
        this.value = valueString;
        this.optionSummaries = optionSummariesList;
        this.status = status;

    }




}
