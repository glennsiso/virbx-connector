package de.glenniso.garmin.virbx.model;

/**
 * Created by glenn on 19.10.15.
 */
public enum FeatureStatus {

    ENABLED, DISABLED, UNKNOWN

}
